# TooltipJS
### By  *Subham D Williams*
###### Hey Guys...This is my next project....This is about Tooltips which are very crucial components of websites....
So I wrote a simple code to help you create dynamic tooltips as easily as possible....
## Getting Started
*To just get started all you have to do is just copy the file [TooltipJS.html](https://github.com/lm10-sd10/TooltipJS/blob/master/TooltipJS.html) and run in your browser...By that you will get a insight of how this actually works....*
## Documentation....
TooltipJS works wit just 3 data-* attributes...These are:<br />
*1)data-tip* : The main attribute in which you can give the text that is going to be showed in the tooltip....<br />
eg :-
```html
<p data-tip=" Hi,I'm the tooltip text...">
  Hover On Me To See The Tooltip....
</p>

```
The default tooltip will be placed 15px down and 15px right relative to the mouse position....
But it may be that you might want to change that positioning by yourself...That's why the other two attributes are used...<br />
*2) data-tip-x* : It specifies the x co-ordinate of the tooltip from the current mouse position....<br />
eg :-
```html
<p data-tip=" Hi,I'm the tooltip text..." data-tip-x="50">
  Hover On Me To See The Tooltip....
</p>

```
*3) data-tip-y* : It specifies the y co-ordinate of the tooltip from the current mouse position....<br />
eg :-
```html
<p data-tip=" Hi,I'm the tooltip text..." data-tip-y="50">
  Hover On Me To See The Tooltip....
</p>

```
You can use the both attributes together...
eg :-
```html
<p data-tip=" Hi,I'm the tooltip text..." data-tip-x="50" data-tip-y="50">
  Hover On Me To See The Tooltip....
</p>

```

      