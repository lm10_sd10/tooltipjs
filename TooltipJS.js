//Attributes to use:
//data-tip for the tool-tip data...
//data-tip-x for the x position relative to the mouse position of the tooltip....
//data-tip-y for the y position relative to the mouse position of the tooltip....
var ttip=$('<span></span>').addClass("ttip");
$('body').append(ttip);
$('*').not('script').not('.ttip').mousemove(function(event) {
	var node=event.target;
	
	if (node.nodeName=="BODY" || node.nodeName=="") {
		$('.ttip').html('');
		$('.ttip').css('display','none');
		
	}
	else{
		if(event.target.hasAttribute("data-tip")) {
			var tip=$(node).attr("data-tip");
			$('.ttip').html(tip);
			$('.ttip').css('display','inline-flex');
			$('.ttip').css('left',event.pageX+15);
			$('.ttip').css('top',event.pageY+15);
			if(event.target.hasAttribute("data-tip-x")) {
				var posx=$(event.target).attr('data-tip-x');
				var posx2=parseInt(posx);
				$('.ttip').css('left',event.pageX+posx2);
			}
			if(event.target.hasAttribute("data-tip-y")) {
				var posy=$(event.target).attr('data-tip-y');
				var posy2=parseInt(posy);
				$('.ttip').css('top',event.pageY+posy2);
			}	
		}
		else{$('.ttip').css('display','none');}		
	}	
});
$('html').hover(function() {
	$('.ttip').html('');
	$('.ttip').css('display','none');
});